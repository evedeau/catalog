import os
import json
import pandas as pd


class Testcase():

    OUTPUT_FOLDER_NAME = "catalog"
    INPUT_FOLDER_NAME = "documentation"
    DEFAULT_TESTCASE_FILENAME = "test_cases.json"
    DEFAULT_SYSTEMVALID_FILENAME = "system_valid.json"
    DEFAULT_SYSTEMCOEXVALID_FILENAME = "system_coex_validation.json"
    DEFAULT_COEX_FILENAME = "ap_coex.json"
    DEFAULT_EXCEL_EXPORTED_FILENAME = "excel_exported_not_formatted.json"

    DEFAULT_URL = "https://qorvo.sharepoint.com/:x:/r/sites/APPS-SWDWQorvoteam/_layouts/15/Doc.aspx?sourcedoc=%7BC8278985-ABD1-4229-97AD-3148FA846867%7D&file=ccc_uwb_test_cases_internal.xlsx&action=default&mobileredirect=true&DefaultItemOpen=1"

    def __init__(self, app):
        self.logger = app.logger
        self.app = app

        self.inputfile = app.args.inputfile
        self.logger.info("Excelfile to convert: {}".format(self.inputfile))


    def init(self):
        cwd = os.getcwd()
        if not self.inputfile.endswith('.xlsx'):
            self.logger.error("Invalid input file: {} - not xlsx file".format(self.inputfile))
            return False

        self.excelFilePath = os.path.join(cwd, Testcase.INPUT_FOLDER_NAME, self.inputfile)
        self.logger.info("Trying to detect excel file: {} ...".format(self.excelFilePath))
        if not os.path.isfile(self.excelFilePath):
            self.logger.error("Missing input file: {}".format(self.excelFilePath))
            return False
        else:
            self.logger.info("input file correctly detected.")
        
        self.outputFolder = os.path.join(cwd, Testcase.OUTPUT_FOLDER_NAME)
        if not os.path.exists(Testcase.OUTPUT_FOLDER_NAME):
            os.makedirs(Testcase.OUTPUT_FOLDER_NAME)
            self.logger.info("{} folder created for output files".format(Testcase.OUTPUT_FOLDER_NAME))

        if self.app.args.outputfile:
            if not self.app.args.outputfile.endswith('.json'):
                self.logger.error("Invalid output filename: {} - not end with .json".format(self.app.args.outputfile))
                return False
            self.outputfile = os.path.join(cwd, self.app.args.outputfile)
        else:
            self.outputfile = os.path.join(cwd, Testcase.OUTPUT_FOLDER_NAME, Testcase.DEFAULT_TESTCASE_FILENAME)
        
        self.outputsystemfile = os.path.join(cwd,Testcase.OUTPUT_FOLDER_NAME, Testcase.DEFAULT_SYSTEMVALID_FILENAME)
        self.outputsystemcoexfile = os.path.join(cwd,Testcase.OUTPUT_FOLDER_NAME, Testcase.DEFAULT_SYSTEMCOEXVALID_FILENAME)
        self.outputapcoexfile = os.path.join(cwd,Testcase.OUTPUT_FOLDER_NAME, Testcase.DEFAULT_COEX_FILENAME)

        return True

    def downloadInputFile(self):
        #download input file from sharepoint
        pass

    def exportExcelData(self, sheet: str):
        dtype_dict = { "priority" : str, "nb_rr" : str, "nb_active_resp": str, "selected_uwb_channel" : str, "resp_slot_n" : str, "nb_recover_iteration" : str, "criteria":str} #needed to map priority (2:mandatory, 1:optionnal)
        # datasheet for test :
        #data = pd.read_excel(self.excelFilePath, "testForScript", skiprows = 6, usecols=range(1,27), header = 0, dtype = dtype_dict)
        # datasheet to handle :
        data = pd.read_excel(self.excelFilePath, sheet, skiprows = 6, usecols=range(1,30), header = 0, dtype = dtype_dict)

        #data['priority'] = data['priority'].map(lambda x: x.replace("2","Mandatory").replace("1","Optional"))

        json_records = data.to_json(orient ='records') 

        # maybe optionnal ?
        parsed = json.loads(json_records)
        outputFile = os.path.join(self.outputFolder, Testcase.DEFAULT_EXCEL_EXPORTED_FILENAME+sheet)
        Testcase.exportDataToJson(outputFile, parsed)
        return outputFile

    def createCatalog(self, file, publishkey):
        with open(file) as inputfile:
            d = json.load(inputfile)
            catalog = dict()

            for test in d:
                if test[publishkey] == 1:
                    testname = test['testname']
                    #create a dict for this test with useful elements well displayed
                    tc = dict()
                    tc['function'] = test['function']
                    tc['tests'] = test['criteria'].split() 
                    tc['priority'] = test["priority"]
                    tc["nb_rr"] = test["nb_rr"] 
                    tc['nb_active_resp'] = test["nb_active_resp"]
                    if test["resp_slot_n"] != None:
                        tc['resp_slot_n'] = test["resp_slot_n"]
                    if test["nb_recover_iteration"] != None:
                        tc['nb_recover_iteration'] = test["nb_recover_iteration"]
                    if test["AP coex"] != None:
                        tc['AP coex'] = test["AP coex"]

                    if type(test["number_responders_nodes"]) == int:
                        test["number_responders_nodes"] = str(hex(test["number_responders_nodes"]))
                    if type(test["uwb_session_id"]) == int or float:
                        test["uwb_session_id"] = str(hex(int(test["uwb_session_id"])))
                    else:
                        print(type(test["uwb_session_id"]))

                    ccc_config = test
                    del ccc_config['testname']
                    del ccc_config['nb_active_resp']
                    del ccc_config['nb_recover_iteration']
                    del ccc_config['resp_slot_n']
                    del ccc_config['criteria']
                    del ccc_config['nb_rr']
                    del ccc_config['priority']
                    del ccc_config['description']
                    del ccc_config['comment']
                    del ccc_config['function']
                    del ccc_config['test_cases.json']
                    del ccc_config['AP coex']
                    del ccc_config['system_valid.json']
                    del ccc_config['system_coex_validation.json']
                    tc["ccc_config"] = ccc_config

                    catalog[testname] = tc
        return catalog


    def execute(self):
        exceldata_test_cases = self.exportExcelData("Test Cases")
        exceldata_ap_coex = self.exportExcelData("Ap coex")
        test_case = self.createCatalog(exceldata_test_cases, "test_cases.json")
        system_valid = self.createCatalog(exceldata_test_cases, "system_valid.json")
        system_coex = self.createCatalog(exceldata_test_cases, "system_coex_validation.json")
        ap_coex = self.createCatalog(exceldata_ap_coex, "test_cases.json")

        Testcase.exportDataToJson(self.outputfile, test_case)
        Testcase.exportDataToJson(self.outputsystemfile, system_valid)
        Testcase.exportDataToJson(self.outputsystemcoexfile, system_coex)
        Testcase.exportDataToJson(self.outputapcoexfile, ap_coex)
        self.logger.info("Test cases have been corectly converted in json file")

    def exportDataToJson(filepath, data):
        with open(filepath, 'w') as outfile:
            json.dump(data, outfile, indent=4)