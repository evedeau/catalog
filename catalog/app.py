from catalog_python import CatCatalog
import os, sys
import argparse
import logging
from testcase import Testcase

class Jsongen():
    
    NAME = "jsongen"
    DESCRIPTION = "Generate json file from excel test case file"

    STATE_INACTIVE = 'INACTIVE'
    STATE_INITIALIZED = 'INITIALIZED'
    STATE_STARTED = 'STARTED'

    EXIT_SUCCESS = 0
    EXIT_NOT_INITIALIZED = -1
    
    def __init__(self, customArgs=None):
        """Constructor."""
        self.state = Jsongen.STATE_INACTIVE
        self.args = None
        self.logger = Jsongen.getLogger("APP")

        self.parser= argparse.ArgumentParser()
        self.customArgs = customArgs
        self.addArguments()


    def addArguments(self):
        self.parser.add_argument('-v', '--verbose',
                                 help='<Optional> Increase verbosity',
                                 action='store_true')

        self.parser.add_argument('-i', '--inputfile',
                                 help='''Excel filename to convert to json in input folder''',
                                 required=True)
                                    
        self.parser.add_argument('-o', '--outputfile',
                                 help='''Output filename in output folder : test_case.json by default''',
                                 required=False)

        self.parser.add_argument('-m', '--mode',
                                 help='''mode json or py : catalog exported as json or py for cat''',
                                 required=False)

        self.args = self.parser.parse_args(self.customArgs)

        if not self.args.inputfile:
            self.parser.error("need and input excel file to convert to JSON")
            self.parser.print_help()
        if not self.args.mode:
            self.parser.error("need to export catalog as json or python")
            self.parser.print_help()
                                    
    def getLogger(name):
        """initLogger method definition."""
        logger = logging.getLogger(name)
        if len(logger.handlers) < 1:
            streamHandler = logging.StreamHandler()
            logFormat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
            formatter = logging.Formatter(logFormat)
            streamHandler.setFormatter(formatter)

            logger.addHandler(streamHandler)
        return logger

    def init(self):
        self._initLogger()
        if self.args.mode == "json":
            self.testcase = Testcase(self)
            if not self.testcase.init():
                return False
        if self.args.mode == "py":
            self.catalog = CatCatalog(self)
            if not self.catalog.init():
                return False

        self._setState(Jsongen.STATE_INITIALIZED)
        self.logger.info("app initializzed")

    def _initLogger(self):
        logLevel = logging.DEBUG
        if self.args.verbose:
            logLevel = logging.DEBUG
        self.logger.setLevel(logLevel)

    def _setState(self, newState):
        if newState != self.state:
            self.state = newState
            self.logger.info('Application state changed to: %s', str(newState))
        else:
            self.logger.warning('Application already in state: %s', str(newState))

    def execute(self):
        """Launch the program (init + start)"""
        errorCode = Jsongen.EXIT_SUCCESS

        self.init()

        if self.state == Jsongen.STATE_INITIALIZED:
            self._setState(Jsongen.STATE_STARTED)
            errorCode = self._execute()
        else:
            self.logger.error("Can not initialize the application.")
            errorCode = Jsongen.EXIT_NOT_INITIALIZED

        self.shutdown(errorCode)       

    def _execute(self):
        """Execute."""
        try:
            if self.args.mode == "json":
                self.logger.info("Starting to extract test case from exel...")
                self.testcase.execute()
                # same than used in pycccvalidation
                return Jsongen.EXIT_SUCCESS
            if self.args.mode == "py":
                self.logger.info("Starting to extract test case from exel...")
                self.catalog.execute()
                return Jsongen.EXIT_SUCCESS
        except KeyboardInterrupt:
            self.logger.critical("Ctrl + c caught, exiting")
            self.shutdown()

    def stop(self):
        #self.testcase.stop()
        pass

    def shutdown(self, errorCode):
        """Shutdown method."""
        if self.state == Jsongen.STATE_STARTED:
            self.stop()
            sys.exit(0)
            self._setState(Jsongen.STATE_INITIALIZED)

        sys.exit(errorCode)
        
def main():
    """
    Main function
    """
    tc = Jsongen()
    tc.execute()


if __name__ == '__main__':
    main()

