import os
import json
import pandas as pd


class CatCatalog():

    OUTPUT_FOLDER_NAME = "generated"
    INPUT_FOLDER_NAME = "documentation"

    DEFAULT_TESTCASE_FILENAME = "catalog_ccc.json"
    DEFAULT_EXCEL_EXPORTED_FILENAME = "excel_exported_not_formatted.json"


    def __init__(self, app):
        self.logger = app.logger
        self.app = app

        self.inputfile = app.args.inputfile
        self.logger.info("Excelfile to convert: {}".format(self.inputfile))


    def init(self):
        cwd = os.getcwd()
        if not self.inputfile.endswith('.xlsx'):
            self.logger.error("Invalid input file: {} - not xlsx file".format(self.inputfile))
            return False

        self.excelFilePath = os.path.join(cwd, CatCatalog.INPUT_FOLDER_NAME, self.inputfile)
        self.logger.info("Trying to detect excel file: {} ...".format(self.excelFilePath))
        if not os.path.isfile(self.excelFilePath):
            self.logger.error("Missing input file: {}".format(self.excelFilePath))
            return False
        else:
            self.logger.info("input file correctly detected.")
        
        self.outputFolder = os.path.join(cwd, CatCatalog.OUTPUT_FOLDER_NAME)
        if not os.path.exists(CatCatalog.OUTPUT_FOLDER_NAME):
            os.makedirs(CatCatalog.OUTPUT_FOLDER_NAME)
            self.logger.info("{} folder created for output files".format(CatCatalog.OUTPUT_FOLDER_NAME))

        if self.app.args.outputfile:
            if not self.app.args.outputfile.endswith('.py'):
                self.logger.error("Invalid output filename: {} - not end with .json".format(self.app.args.outputfile))
                return False
            self.outputfile = os.path.join(cwd, self.app.args.outputfile)
        else:
            self.outputfile = os.path.join(cwd, CatCatalog.OUTPUT_FOLDER_NAME, CatCatalog.DEFAULT_TESTCASE_FILENAME)
        
        return True

    def downloadInputFile(self):
        #download input file from sharepoint
        pass


    def exportExcelData(self, sheet: str):
        dtype_dict = { "priority" : str, "nb_rr" : str, "nb_active_resp": str, "selected_uwb_channel" : str, "resp_slot_n" : str, "nb_recover_iteration" : str, "criteria":str} #needed to map priority (2:mandatory, 1:optionnal)
        # datasheet for test :
        #data = pd.read_excel(self.excelFilePath, "testForScript", skiprows = 6, usecols=range(1,27), header = 0, dtype = dtype_dict)
        # datasheet to handle :
        data = pd.read_excel(self.excelFilePath, sheet, skiprows = 6, usecols=range(1,36), header = 0, dtype = dtype_dict)

        #data['priority'] = data['priority'].map(lambda x: x.replace("2","Mandatory").replace("1","Optional"))

        json_records = data.to_json(orient ='records') 

        # maybe optionnal ?
        parsed = json.loads(json_records)
        outputFile = os.path.join(self.outputFolder, CatCatalog.DEFAULT_EXCEL_EXPORTED_FILENAME+sheet)
        CatCatalog.exportDataToJson(outputFile, parsed)
        return outputFile


    def write_catalog(self, file, publishkey):
        f = open("catalog_ccc.py","a")
        with open(file) as inputfile:
            d = json.load(inputfile)

            for test in d:                    
                if test[publishkey] == 1:
                    testcontent = self.build_test_content(test)
                    f.write(f"\n{testcontent}")
        f.close()
    
    def build_test_content(self,test):
        #print(test)
        ccc_conf=f'\n\t\t\t"nb_active_resp":"{test["nb_active_resp"]}",'\
            f'\n\t\t\t"number_responders_nodes":"{test["number_responders_nodes"]}",'\
            f'\n\t\t\t"number_chaps_per_slot":"{test["number_chaps_per_slot"]}",'\
            f'\n\t\t\t"number_slot_per_round":"{test["number_slot_per_round"]}",'\
            f'\n\t\t\t"ran_multiplier":"{test["ran_multiplier"]}",'\
            f'\n\t\t\t"selected_uwb_channel":"{test["selected_uwb_channel"]}",'\
            f'\n\t\t\t"sts_index0":"{test["sts_index0"]}",'\
            f'\n\t\t\t"hop_mode":"{test["hop_mode"]}",'\
            f'\n\t\t\t"hop_mode_key":"{test["hop_mode_key"]}",'\
            f'\n\t\t\t"selected_protocol_version":"{test["selected_protocol_version"]}",'\
            f'\n\t\t\t"selected_uwb_config_id":"{test["selected_uwb_config_id"]}",'\
            f'\n\t\t\t"sync_code_index":"{test["sync_code_index"]}",'\
            f'\n\t\t\t"uwb_session_id":"{test["uwb_session_id"]}",'\
            f'\n\t\t\t"uwb_time0":"{test["uwb_time0"]}",'\
            f'\n\t\t\t"URSK":"{test["URSK"]}",'\
            f'\n\t\t\t"selected_pulseshape_combo":"{test["selected_pulseshape_combo"]}"'

        args=f'\n\t\t\tname="{test["testname"]}",'\
            f'\n\t\t\ttest_type="{test["function"]}",'\
            f'\n\t\t\tnb_rr={test["nb_rr"]},'\
            f'\n\t\t\tccc_config={{{ccc_conf}\n\t\t\t}}'

        testclass=None
        if test["function"]=="ccc_mac_generic":
            testclass="CccGeneric"
        elif test["function"]=="ccc_mac_adaptative":
            testclass="CccAdaptative"
        else:
            testclass="undefined"

        frame=f"{test['testname']} = {testclass}({args})"
        return frame

    def execute(self):
        exceldata_test_cases = self.exportExcelData("Test Cases")
        self.write_catalog(exceldata_test_cases, publishkey="test_cases.json")

        self.write_campaign(exceldata_test_cases, "ccc_campaign_all.py", publishkey="test_cases.json")
        self.write_campaign(exceldata_test_cases, "ccc_campaign_ci_light.py", publishkey="ci_light.json")

        self.logger.info("Test cases have been corectly converted in json file")

    def exportDataToJson(filepath, data):
        with open(filepath, 'w') as outfile:
            json.dump(data, outfile, indent=4)

    def write_campaign(self, data, file, publishkey):
        f = open(file,"a")
        with open(data) as inputfile:
            d = json.load(inputfile)

            for test in d:                    
                if test[publishkey] == 1:
                    testcontent = self.add_test(test)
                    f.write(f"{testcontent}")
        f.close()

    def add_test(self,test):
        frame=f"\n\tcampaign.add({test['testname']})"
        return frame